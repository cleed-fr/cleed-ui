# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.9.3](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.9.2...v1.9.3) (2020-10-20)


### Features

* **product-card:** increase grey darkness ([1c0bc17](https://bitbucket.org/cleed-fr/cleed-ui/commit/1c0bc17c2d69e27c84f4c803467a26982146b481))

### [1.9.2](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.9.1...v1.9.2) (2020-10-20)


### Bug Fixes

* **product-card:** decrease text font size on small mode ([32712bf](https://bitbucket.org/cleed-fr/cleed-ui/commit/32712bf4d6c476e79e7aa8dd2028ebdad1ab8038))

### [1.9.1](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.9.0...v1.9.1) (2020-10-20)


### Bug Fixes

* **product-card:** invert text size when small flag is on ([741198c](https://bitbucket.org/cleed-fr/cleed-ui/commit/741198c21740eb9ee8751a5ce449d1fb95768396))

## [1.9.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.8.0...v1.9.0) (2020-10-20)


### Features

* **product-card:** add a small prop to reduce text font ([eddd118](https://bitbucket.org/cleed-fr/cleed-ui/commit/eddd11832b4f6111f79047525280b47c51dab451))
* **product-card:** reduce description text font when small flag is on ([0999adf](https://bitbucket.org/cleed-fr/cleed-ui/commit/0999adf70c8a5d181b952573d8e87f7657fae908))

## [1.8.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.7.0...v1.8.0) (2020-10-19)


### Features

* **product-card:** emit event on action button click ([2f1d0c4](https://bitbucket.org/cleed-fr/cleed-ui/commit/2f1d0c4ca6a40824560c91c6facf6c29db31e0c2))

## [1.7.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.6.0...v1.7.0) (2020-10-16)


### Features

* **product-card:** add tooltip to actions buttons ([33add5e](https://bitbucket.org/cleed-fr/cleed-ui/commit/33add5ea4cb5af3a107c69dc3a9bafaa7d4e9054))

## [1.6.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.5.0...v1.6.0) (2020-10-15)


### Features

* **product-card:** add a fullHeight prop to conditionnaly apply full height constraint ([a3e916f](https://bitbucket.org/cleed-fr/cleed-ui/commit/a3e916fdfd26ac805c94b7a196decc17bdda5989))

## [1.5.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.4.0...v1.5.0) (2020-10-14)


### Features

* **product-card:** make card title extensible ([24111cc](https://bitbucket.org/cleed-fr/cleed-ui/commit/24111ccd81bd87d2eb872ea6071aa0f394e12a54))

## [1.4.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.3.0...v1.4.0) (2020-10-09)


### Features

* enable link customization ([749f9be](https://bitbucket.org/cleed-fr/cleed-ui/commit/749f9bec1faf1b552c09e0d48254d6e377488080))

## [1.3.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.2.0...v1.3.0) (2020-08-26)


### Features

* **product-card:** add space between the image and the rest ([bc6a50e](https://bitbucket.org/cleed-fr/cleed-ui/commit/bc6a50e6b4165878a906387322bee656d4e6fce0))

## [1.2.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.1.0...v1.2.0) (2020-08-04)


### Features

* **product-card:** add raised mode ([737f3f9](https://bitbucket.org/cleed-fr/cleed-ui/commit/737f3f9f23699928053f9a0ba5ffcfd1a308d560))

## [1.1.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v1.0.0...v1.1.0) (2020-08-03)


### Features

* **product-card:** limit card title to one line ([492b9d7](https://bitbucket.org/cleed-fr/cleed-ui/commit/492b9d7b728edd47edbbe9f9b20ed274a55c9fc0))
* add a prop to capitalize button text ([be499a7](https://bitbucket.org/cleed-fr/cleed-ui/commit/be499a7b5a02c657290d0d8ae83800a364e722eb))
* add blog article thumbnail ([81795b4](https://bitbucket.org/cleed-fr/cleed-ui/commit/81795b463d49d3de66420b26aba7cb57c86c3c2c))
* add grey mode to product card ([9e14d1a](https://bitbucket.org/cleed-fr/cleed-ui/commit/9e14d1a48bc2687bcc9348aa724670943b34a430))
* add minWidth prop to product card ([54bf47f](https://bitbucket.org/cleed-fr/cleed-ui/commit/54bf47f91148f8957440c9e79a2d93d4a3078ac8))
* add outlined prop to article ([f3c41e8](https://bitbucket.org/cleed-fr/cleed-ui/commit/f3c41e810b5afb923f151a673e9d9866cebc086a))
* add secondary mode to blog article thumbnail ([1cb2cb0](https://bitbucket.org/cleed-fr/cleed-ui/commit/1cb2cb0aa7de30e25ec66e84bdd1ae9c4b418b8b))
* add showBrand prop ([13470e5](https://bitbucket.org/cleed-fr/cleed-ui/commit/13470e54338c965444569790b543de1f42362cc5))
* add small mode to product card ([597d491](https://bitbucket.org/cleed-fr/cleed-ui/commit/597d491310ee2f5f3030b6650127e28ffaf80b10))
* **product-card:** let maxWidth be a prop ([13e5e8e](https://bitbucket.org/cleed-fr/cleed-ui/commit/13e5e8e06596e2e07ffe299a9b44174807e7d065))

## [1.0.0](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.4...v1.0.0) (2020-05-26)

### [0.1.4](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.3...v0.1.4) (2020-05-26)

### [0.1.5](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.3...v0.1.5) (2020-05-26)

### [0.1.4](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.3...v0.1.4) (2020-05-26)

### [0.1.4](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.3...v0.1.4) (2020-05-26)

### [0.1.3](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.2...v0.1.3) (2020-05-26)

### [0.1.2](https://bitbucket.org/cleed-fr/cleed-ui/compare/v0.1.1...v0.1.2) (2020-05-26)

### 0.1.1 (2020-05-26)


### Features

* add ProductCard component and implement Storybook ([ab1d320](https://bitbucket.org/cleed-fr/cleed-ui/commit/ab1d320f8d72f4a97c904742ada6b2789a8de006))
* init Vue CLI project ([6931d51](https://bitbucket.org/cleed-fr/cleed-ui/commit/6931d51f771518dfc2383d1f9e9ca515accbc00a))

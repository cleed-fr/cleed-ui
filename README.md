# cleed-ui

## Project setup

```
npm install
```

### Starts Storybook

```
npm run serve:storybook
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Make a new release

```
npm run release
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

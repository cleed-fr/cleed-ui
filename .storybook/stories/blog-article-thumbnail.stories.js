import { text, number } from '@storybook/addon-knobs'

import BlogArticleThumbnail from '../../components/BlogArticleThumbnail.vue'

export default { title: 'BlogArticleThumbnail' }

const post = () => ({
  thumbnailImage: {
    default: text('Thumbnail URL', require('../../assets/img/carreaux.png')),
  },
  title: {
    default: text('Title', 'Les carreaux, plus que jamais !'),
  },
  description: {
    default: text(
      'Description',
      'Cette saison, la mode fait cap sur l’Écosse ! En effet, les carreaux reprennent du…'
    ),
  },
  createdAt: {
    default: text('Timestamp', '2020-07-02T15:58:36.028Z'),
  },
  slug: {
    default: text('Slug', 'carreaux'),
  },
})

export const withBase = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" />
  `,
})

export const withSecondary = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" secondary />
  `,
})

export const withCapitalize = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" secondary button-capitalize />
  `,
})

export const withOutlined = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" outlined />
  `,
})

export const withOutlinedAndSecondary = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" secondary outlined />
  `,
})

export const withOutlinedAndSecondaryAndCapitalize = () => ({
  components: { BlogArticleThumbnail },
  props: {
    ...post(),
  },
  template: `
    <blog-article-thumbnail v-bind="{ thumbnailImage, title, description, createdAt, slug }" secondary outlined button-capitalize />
  `,
})

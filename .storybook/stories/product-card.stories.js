import { text, number } from '@storybook/addon-knobs'

import ProductCard from '../../components/ProductCard.vue'

export default { title: 'ProductCard' }

const product = () => ({
  _id: {
    default: text('Product ID', 'DMyz1_RE3BQM2RPz9ov5JX2LeRbv3YYbcttCpWZ4iag'),
  },
  brand: {
    default: text('Product brand', 'Ollygan'),
  },
  name: {
    default: text('Product name', 'Veste bleue Bill'),
  },
  description: {
    default: text(
      'Product description',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eu arcu nisl. Duis vitae luctus mi. Nam ut malesuada nisl, vel bibendum nisl. Nam tempus tempus magna eu auctor.'
    ),
  },
  pictureUrl: {
    default: text(
      'Picture URL',
      'https://www.ollygan.com/media/catalog/product/cache/068ac46b4b3cfcc466271bbb647458f6/2/8/28340-bleu-9912.jpg'
    ),
  },
  price: {
    default: number('Product price', 249),
  },
})

export const withDistributor = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" show-distributor />
  `,
})

export const withDescription = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" />
  `,
})

export const withBrand = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" show-brand />
  `,
})

export const inGrey = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" grey />
  `,
})

export const withSmall = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" small />
  `,
})

export const withSmallText = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" small-text />
  `,
})

export const withSmallAndDistributor = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" small show-distributor />
  `,
})

export const withRaise = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" raised />
  `,
})

export const withRaiseAndDistributor = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" raised show-distributor />
  `,
})

export const multipleCardsInRow = () => ({
  components: { ProductCard },
  props: {
    ...product(),
  },
  template: `
    <div class="d-flex">
      <v-col
        cols="12"
        md="3"
        class="pa-2"
      >
        <product-card v-bind="{ _id, brand, name, description, pictureUrl, price }" raised show-distributor full-height />
      </v-col>

      <v-col
        cols="12"
        md="3"
        class="pa-2"
      >
        <product-card v-bind="{ _id, brand, description, pictureUrl, price }" name="Lorem ipsum dolor sit amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet amet" raised show-distributor full-height />
      </v-col>
    </div>
  `,
})

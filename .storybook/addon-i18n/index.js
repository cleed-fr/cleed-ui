import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

Vue.mixin({
  methods: {
    localePath(path) {
      return path
    },
  },
})

export default new VueI18n({
  locale: 'en',
})
